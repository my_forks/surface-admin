<?php

namespace app\backend\controller\attachment;

use iszsw\curd\Helper;
use app\backend\model\Attachment as AttachmentModel;
use surface\form\components\Checkbox;
use surface\form\components\Date;
use surface\form\components\Input;
use surface\form\components\Slider;
use surface\helper\FormAbstract;

class Search extends FormAbstract
{

    public function rules(): array
    {
        return [
            'name' => 'LIKE',
            'suffix' => 'IN',
            'size' => 'BETWEEN',
            'create_time' => 'BETWEEN',
        ];
    }


    public function columns(): array
    {
        $fileType = request()->param('fileType/a', null);
        $min = (int)(AttachmentModel::min('size') / 1000);// KB
        $max = (int)(AttachmentModel::max('size') / 1000) + 1;// KB
        return [
            new Input('name', AttachmentModel::$labels['name']),
            (new Checkbox('suffix', AttachmentModel::$labels['suffix'], $fileType ?: []))->options(Helper::formatOptions(AttachmentModel::$typeLabels)),
            (new Slider('size', AttachmentModel::$labels['size'].'(KB)', [0, 0]))->style('minWidth', '200px')->props(['min' => $min, 'max' => $max, 'range' =>true]),
            (new Date('create_time', AttachmentModel::$labels['create_time']))->props(
                [
                    'type'        => "datetimerange",
                    'value-format' => "yyyy-MM-dd HH:mm:ss",
                    'placeholder' => "请选择活动日期",
                ]
            ),
        ];
    }

}
