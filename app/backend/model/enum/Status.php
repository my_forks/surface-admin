<?php

namespace app\backend\model\enum;

class Status
{
    const STATUS_DISABLE = 0;
    const STATUS_NORMAL = 1;
}
